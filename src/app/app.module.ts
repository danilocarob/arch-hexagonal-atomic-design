import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { CharacterRepository } from './character/domain/characterRepository';
import { CharacterApiService } from './character/infraestructure/character-api.service';
import { CharactersComponent } from './character/ui/characters/characters.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    CharactersComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [{provide: CharacterRepository, useClass: CharacterApiService}],
  bootstrap: [AppComponent]
})
export class AppModule { }
