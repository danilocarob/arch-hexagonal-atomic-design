import { Injectable } from '@angular/core';
import { CharacterRepository } from '../domain/characterRepository';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Character } from '../domain/character';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CharacterApiService extends CharacterRepository {
 private _url = environment.apiUrl;

  constructor( private http: HttpClient) { super(); }

  getByID(id: number): Observable<Character> {
    return this.http.get<Character>(this._url+id);
  }
  
  getAllCharacters(): Observable<Character[]> {
    return this.http.get<Array<Character>>(this._url);
  }
}
