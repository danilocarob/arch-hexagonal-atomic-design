import { Component, OnInit } from '@angular/core';
import { CharacterRepository } from '../../domain/characterRepository';

@Component({
  providers: [],
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.css']
})
export class CharactersComponent implements OnInit {
  public response : any;
  public datos : any;

  constructor( private _getCharacters : CharacterRepository) { }

  ngOnInit(): void {
    this.response = this._getCharacters.getAllCharacters();
    this.response.subscribe (
      (data : any) => {
        console.log(data);
        
        this.datos = data.results;
        console.log('datos',this.datos);
        
      }
    );
    
  }

}
