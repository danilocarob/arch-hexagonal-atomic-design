export class Character {
    id: number = 0;
    name: string = "";
    status: string = "";
    species: string = "";
    type: string = "";
    gender: string = "";
    origin: any
}