import { Observable } from 'rxjs';
import { Character } from './character';

export abstract class CharacterRepository {
    abstract getByID(id: number): Observable<Character>;
    abstract getAllCharacters(): Observable<Array<Character>>;
} 