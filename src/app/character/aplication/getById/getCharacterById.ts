import { Inject, inject, Injectable } from '@angular/core';
import { from, Observable } from 'rxjs';
import { Character } from '../../domain/character';
import { CharacterRepository } from '../../domain/characterRepository';


@Injectable({
  providedIn: 'root'
})

export class getCharacterById {
  constructor( private _characterRepository: CharacterRepository) {}

  getByID (id: number) : Observable <Character> {
    return this._characterRepository.getByID(id);
  }
}