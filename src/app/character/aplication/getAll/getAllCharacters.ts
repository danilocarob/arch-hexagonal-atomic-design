import { Inject, inject, Injectable } from '@angular/core';
import { from, Observable } from 'rxjs';
import { Character } from '../../domain/character';
import { CharacterRepository } from '../../domain/characterRepository';


@Injectable({
  providedIn: 'root'
})

export class getAllCharacters {
  constructor( private _characterRepository: CharacterRepository) {}
  
  getAllCharacters (id: number) : Observable <Character> {
    return this._characterRepository.getByID(id);
  }
}